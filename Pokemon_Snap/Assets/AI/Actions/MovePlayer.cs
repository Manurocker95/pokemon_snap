using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using RAIN.Action;
using RAIN.Core;
using PokemonSnapUnity;

[RAINAction]
public class MovePlayer : RAINAction
{
    public override void Start(RAIN.Core.AI ai)
    {
        base.Start(ai);
    }

    public override ActionResult Execute(RAIN.Core.AI ai)
    {
        //GameObject nextStep = ai.WorkingMemory.GetItem<GameObject>("nextStep");
        float moveSpeed = ai.WorkingMemory.GetItem<float>("moveSpeed");

        //PokemonSnapUnity.Snap_PhotoManager.Instance.PlayerScript.MoveToPoint(nextStep.transform.position, moveSpeed);
        return ActionResult.SUCCESS;
    }

    public override void Stop(RAIN.Core.AI ai)
    {
        base.Stop(ai);
    }
}