﻿/// This code is personal and can't be used for commercial games.
/// Non-profit projects can use it with explicit permission and credits
/// 
/// Made by Manuel Rodríguez Matesanz.
/// You can contact me on GBATemp as Manurocker95, email: Manuelrodriguezmatesanz@gmail.com
/// or even on my web: Manuelrodriguezmatesanz.com
/// 

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PokemonSnapUnity
{
    /// <summary>
    /// Small script that moves an object around the screen with constant speed. 
    /// It's used for Pikachu and Todd Initial Animation
    /// </summary>
    [RequireComponent(typeof(Collider2D))]
    public class Snap_MenuSpriteMove : MonoBehaviour
    {
        #region Variables
        /// <summary>
        /// Movement Speed in X Axis
        /// </summary>
        [SerializeField] private float m_movementSpeedX = -0.1f;
        /// <summary>
        /// Movement Speed in Y Axis
        /// </summary>
        [SerializeField] private float m_movementSpeedY = 0f;
        /// <summary>
        /// Original Position 
        /// </summary>
        private Vector3 m_originalPosition;
        /// <summary>
        /// If this GO can move or not
        /// </summary>
        private bool m_canMove = true;
        #endregion

        #region Monobehaviour
        // Use this for initialization
        void Start()
        {
            m_originalPosition = new Vector3(transform.position.x, transform.position.y, transform.position.z);
        }

        // Update is called once per frame
        void Update()
        {
            if (m_canMove)
                transform.position = new Vector3(transform.position.x + m_movementSpeedX, transform.position.y + m_movementSpeedY, transform.position.z);
        }

        private void OnCollisionEnter2D(Collision2D collision)
        {
            gameObject.SetActive(false);
        }

        public void SetOriginalPosition()
        {
            transform.position = m_originalPosition;
        }
        #endregion
    }

}