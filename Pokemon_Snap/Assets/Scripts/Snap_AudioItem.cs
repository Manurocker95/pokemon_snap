﻿/// This code is personal and can't be used for commercial games.
/// Non-profit projects can use it with explicit permission and credits
/// 
/// Made by Manuel Rodríguez Matesanz.
/// You can contact me on GBATemp as Manurocker95, email: Manuelrodriguezmatesanz@gmail.com
/// or even on my web: Manuelrodriguezmatesanz.com
/// 

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PokemonSnapUnity
{
    /// <summary>
    /// Audioitem is a script that adds all the stuff needed for AudioManager
    /// without the need of setting it manually :D
    /// </summary>
    [RequireComponent(typeof(AudioSource))]
    public class Snap_AudioItem : MonoBehaviour
    {
        #region Variables
        /// <summary>
        /// AudioSource referenced. No need to be the one attached to the current GO but, it should
        /// </summary>
        [SerializeField] AudioSource audioSource;
        /// <summary>
        /// Clip that will override AudioSource's one. So you don't need to worry about it.
        /// </summary>
        [SerializeField] AudioClip myClip;
        /// <summary>
        /// Starting Volume for this audioSource. Remember the volume will be Effect Type * Volume * Master
        /// </summary>
        [SerializeField] [Range(0f, 1f)] float initialVolume = 1f;
        /// <summary>
        /// Unique key for AudioManager Dictionary.
        /// </summary>
        [SerializeField] string sourceName;
        /// <summary>
        /// Variable that sets the type of this audio. SFX, Music...
        /// </summary>
        [SerializeField] Snap_AudioManager.AudioType Type = Snap_AudioManager.AudioType.SFX;
        /// <summary>
        /// If play on Awake is needed
        /// </summary>
        [SerializeField] bool m_playedOnInit = false;
        /// <summary>
        /// If we want to override directly the clip (AudioItem to AudioSource)
        /// </summary>
        [SerializeField] bool m_overrideSourceClip = false;
        /// <summary>
        /// If we want to override the initial volume.
        /// </summary>
        [SerializeField] bool m_overrideVolume = false;
        /// <summary>
        /// If the audioSource is playing in Loop
        /// </summary>
        [SerializeField] bool m_loop = false;
        /// <summary>
        /// If SourceName will use this GO Name
        /// </summary>
        [SerializeField] bool useGOName = false;
        /// <summary>
        /// If the gameobject is an instance from prefab so it can share SourceName
        /// </summary>
        [SerializeField] bool m_instantiating = false;

        public AudioSource source { get { return audioSource; } }
        public string itemName { get { return sourceName; } }
        public bool isPlayedOnInit { get { return m_playedOnInit; } set { m_playedOnInit = value; } }
        public bool OverrideSourceClip { get { return m_overrideSourceClip; } set { m_overrideSourceClip = value; } }
        public bool OverrideVolume { get { return m_overrideVolume; } set { m_overrideVolume = value; } }
        public Snap_AudioManager.AudioType AudioType { get { return Type; } }
        #endregion

        #region Init
        // Use this for initialization
        void Start()
        {
            if (!audioSource)
                audioSource = GetComponent<AudioSource>();

            if (!myClip && audioSource.clip)
                myClip = audioSource.clip;

            if (useGOName)
                sourceName = gameObject.name;

            if (Snap_AudioManager.Instance)
            {
                if (sourceName != "" && sourceName != null)
                    Snap_AudioManager.Instance.AddAudioSource(audioSource, sourceName, Type, myClip, m_loop, initialVolume, m_loop, m_overrideSourceClip, m_instantiating, m_overrideVolume);
                else
                    Debug.LogError("Introduce una key independiente para sourceName en " + gameObject.name);
            }
        }
        #endregion

        #region Volume Methods
        public void ModifyVolume(float mod)
        {
            Snap_AudioManager.Instance.ModifyVolume(sourceName, mod);
        }

        public void SetNewVolume(float volume)
        {
            if (Snap_AudioManager.Instance)
                Snap_AudioManager.Instance.SetSourceVolume(sourceName, volume);
        }

        private void OnDestroy()
        {
            // UHMAudioManager.Instance.RemoveAudioPlaying(sourceName);
        }

        public float GetVolume()
        {
            float vol = Snap_AudioManager.Instance.GetVolume(sourceName);

            if (vol == -1)
                vol = initialVolume;

            return vol;
        }
        #endregion
    }

}