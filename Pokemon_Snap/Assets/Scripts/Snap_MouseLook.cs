﻿/// This code is personal and can't be used for commercial games.
/// Non-profit projects can use it with explicit permission and credits
/// 
/// Made by Manuel Rodríguez Matesanz.
/// You can contact me on GBATemp as Manurocker95, email: Manuelrodriguezmatesanz@gmail.com
/// or even on my web: Manuelrodriguezmatesanz.com
/// 

using System;
using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;

namespace PokemonSnapUnity
{
    /// <summary>
    /// Copy of Standard Assets Mouse Look and adapted for Pokémon Snap
    /// </summary>
    [Serializable]
    public class Snap_MouseLook
    {

        #region Variables
        public float XSensitivity = 2f;
        public float YSensitivity = 2f;
        public float XSensitivityC = 2f;
        public float YSensitivityC = 2f;
        public bool clampVerticalRotation = true;
        public float MinimumX = -90F;
        public float MaximumX = 90F;
        public bool smooth;
        public float smoothTime = 5f;
        public bool lockCursor = true;
        public bool invertedAxis = false;
        public int invertAxisValue = 1;

        private Quaternion m_CharacterTargetRot;
        private Quaternion m_CameraTargetRot;
        private bool m_cursorIsLocked = true;
        #endregion

        #region Initialization
        /// <summary>
        /// Initialize the mouse look with values from Settings manager (if it is in the scene)
        /// </summary>
        /// <param name="character"></param>
        /// <param name="camera"></param>
        public void Init(Transform character, Transform camera)
        {
            m_CharacterTargetRot = character.localRotation;
            m_CameraTargetRot = camera.localRotation;

            if (Snap_SettingsManager.Instance)
            {
                XSensitivity = Snap_SettingsManager.Instance.MouseXSensibility;
                YSensitivity = Snap_SettingsManager.Instance.MouseYSensibility;
                XSensitivityC = Snap_SettingsManager.Instance.ControllerXSensibility;
                YSensitivityC = Snap_SettingsManager.Instance.ControllerYSensibility;
                invertedAxis = Snap_SettingsManager.Instance.InvertYAxis;

                if (invertedAxis)
                    invertAxisValue = -1;
                else
                    invertAxisValue = 1;
            }
        }
        #endregion

        #region Set Values From Settings Manager
        /// <summary>
        /// Set Controller Sensitivity in X and Y Axis
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        public void SetControllerSensibility(float x, float y)
        {
            XSensitivityC = x;
            YSensitivityC = y;
        }

        /// <summary>
        /// Set Mouse Sensitivity in X and Y Axis
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        public void SetMouseSensibility(float x, float y)
        {
            XSensitivity = x;
            YSensitivity = y;
        }

        /// <summary>
        /// Set Invert Axis
        /// </summary>
        /// <param name="inverted"></param>
        public void SetInvertYAxis(bool inverted)
        {
            invertedAxis = inverted;

            if (inverted)
                invertAxisValue = -1;
            else
                invertAxisValue = 1;
        }
        #endregion

        #region Mouse Look Stuff
        /// <summary>
        /// Rotation method
        /// </summary>
        /// <param name="character"></param>
        /// <param name="camera"></param>
        public void LookRotation(Transform character, Transform camera)
        {
            float yRot = CrossPlatformInputManager.GetAxis("Mouse X") * XSensitivity;
            float xRot = CrossPlatformInputManager.GetAxis("Mouse Y") * YSensitivity * invertAxisValue;

            float yJoyRot = 0;
            float xJoyRot = 0;

            if (Snap_InputManager.Instance && Snap_InputManager.Instance.IsUsingController)
            {
                yJoyRot = CrossPlatformInputManager.GetAxis("Horizontal") * XSensitivityC;
                xJoyRot = CrossPlatformInputManager.GetAxis("Vertical") * YSensitivityC * invertAxisValue;
            }

            m_CharacterTargetRot *= Quaternion.Euler(0f, yRot + yJoyRot + yJoyRot, 0f);
            m_CameraTargetRot *= Quaternion.Euler(-xRot - xJoyRot, 0f, 0f);

            if (clampVerticalRotation)
                m_CameraTargetRot = ClampRotationAroundXAxis(m_CameraTargetRot);

            if (smooth)
            {
                character.localRotation = Quaternion.Slerp(character.localRotation, m_CharacterTargetRot,
                    smoothTime * Time.deltaTime);
                camera.localRotation = Quaternion.Slerp(camera.localRotation, m_CameraTargetRot,
                    smoothTime * Time.deltaTime);
            }
            else
            {
                character.localRotation = m_CharacterTargetRot;
                camera.localRotation = m_CameraTargetRot;
            }

            UpdateCursorLock();
        }

        /// <summary>
        /// Lock the cursor 
        /// </summary>
        /// <param name="value"></param>
        public void SetCursorLock(bool value)
        {
            lockCursor = value;
            if (!lockCursor)
            {//we force unlock the cursor if the user disable the cursor locking helper
                Cursor.lockState = CursorLockMode.None;
                Cursor.visible = true;
            }
        }
        /// <summary>
        /// Update cursor lock
        /// </summary>
        public void UpdateCursorLock()
        {
            //if the user set "lockCursor" we check & properly lock the cursos
            if (lockCursor)
                InternalLockUpdate();
        }

        private void InternalLockUpdate()
        {
            if (Input.GetKeyUp(KeyCode.Escape))
            {
                m_cursorIsLocked = false;
            }
            else if (Input.GetMouseButtonUp(0))
            {
                m_cursorIsLocked = true;
            }

            if (m_cursorIsLocked)
            {
                Cursor.lockState = CursorLockMode.Locked;
                Cursor.visible = false;
            }
            else if (!m_cursorIsLocked)
            {
                Cursor.lockState = CursorLockMode.None;
                Cursor.visible = true;
            }
        }

        Quaternion ClampRotationAroundXAxis(Quaternion q)
        {
            q.x /= q.w;
            q.y /= q.w;
            q.z /= q.w;
            q.w = 1.0f;

            float angleX = 2.0f * Mathf.Rad2Deg * Mathf.Atan(q.x);

            angleX = Mathf.Clamp(angleX, MinimumX, MaximumX);

            q.x = Mathf.Tan(0.5f * Mathf.Deg2Rad * angleX);

            return q;
        }
        #endregion
    }

}