﻿/// This code is personal and can't be used for commercial games.
/// Non-profit projects can use it with explicit permission and credits
/// 
/// Made by Manuel Rodríguez Matesanz.
/// You can contact me on GBATemp as Manurocker95, email: Manuelrodriguezmatesanz@gmail.com
/// or even on my web: Manuelrodriguezmatesanz.com
/// 

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PokemonSnapUnity
{
    /// <summary>
    /// Common information shared by all the pokémon
    /// </summary>
    public abstract class Snap_Pokemon : MonoBehaviour
    {
        #region Variables
        /// <summary>
        /// Unique key of the JSON
        /// </summary>
        [SerializeField] protected string m_pokemonKey = "Test";
        /// <summary>
        /// Name of the Pokémon. 
        /// </summary>
        [SerializeField] protected string m_pokemonName = "Test";
        /// <summary>
        /// Some Pokémon like Snorlax can't be photographied til they wake up. 
        /// </summary>
        [SerializeField] protected bool m_canBePhotographied = true;
        /// <summary>
        /// If this GO is considered Signal
        /// </summary>
        [SerializeField] protected bool m_isSignal = false;
        /// <summary>
        /// If it's doing a pose bonus. 1f - Not doing anything. 2f - Affected by pester and stuff
        /// </summary>
        [SerializeField] protected float m_poseMultipier = 1f;
        /// <summary>
        /// If it gives a bonus
        /// </summary>
        [SerializeField] protected bool m_specialBonus = false;
        /// <summary>
        /// Points given now when photographied (So you can set multiple bonuses)
        /// </summary>
        [SerializeField] protected float m_bonusPoints = 1000f;
        /// <summary>
        /// Distance to player
        /// </summary>
        [SerializeField] protected float m_distanceToPlayer = 0f;
        /// <summary>
        /// Minimum distance to be considered as rendered
        /// </summary>
        [SerializeField] protected float m_minimumDistanceToBeRendered = 6f;
        /// <summary>
        /// If the Pokémon is considered rendered or not. 
        /// </summary>
        [SerializeField] protected bool m_isRendered = false;

        /// <summary>
        /// Properties
        /// </summary>
        public string PokemonKey { get { return m_pokemonKey; } }
        public string PokemonName { get { return m_pokemonName; } }
        public float PoseMultiplier { get { return m_poseMultipier; } }
        public float BonusPoints { get { return m_bonusPoints; } }
        public bool SpecialBonus { get { return m_specialBonus; } }
        public bool IsSignal { get { return m_isSignal; } }
        public bool CanBePhotographied { get { return m_canBePhotographied; } }
        public bool IsRendered { get { return m_isRendered; } }
        #endregion

        #region Methods
        protected void Start()
        {
            Snap_EventManager.StartListening(Snap_PhotoManager.Instance.SnapTriggerEvent, CheckIsRenderer);
        }
        /// <summary>
        /// When a snapshot is done, this checks if the camera can see the Pokémon and if it is close enough to be considered
        /// </summary>
        protected void CheckIsRenderer()
        {
            m_distanceToPlayer = (Snap_PhotoManager.Instance.PlayerScript.transform.position - transform.position).magnitude;
            m_isRendered = (m_distanceToPlayer < m_minimumDistanceToBeRendered && GetComponent<Renderer>().isVisible) ? true : false;
        }
        /// <summary>
        /// Action done when hearing the floute
        /// </summary>
        protected void OnFloute() { }
        /// <summary>
        /// Action done when bait is near
        /// </summary>
        protected void OnBait() { }
        /// <summary>
        /// Action done when pesterball is near
        /// </summary>
        protected void OnSmell() { }
        #endregion
    }
}
