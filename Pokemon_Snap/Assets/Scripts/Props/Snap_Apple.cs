﻿/// This code is personal and can't be used for commercial games.
/// Non-profit projects can use it with explicit permission and credits
/// 
/// Made by Manuel Rodríguez Matesanz.
/// You can contact me on GBATemp as Manurocker95, email: Manuelrodriguezmatesanz@gmail.com
/// or even on my web: Manuelrodriguezmatesanz.com
/// 

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PokemonSnapUnity
{
    public class Snap_Apple : MonoBehaviour
    {
        [SerializeField] private Rigidbody m_rigidbody;
        [SerializeField] private Collider m_collider;
        [SerializeField] private AudioSource m_audioSource;
        [SerializeField] private AudioClip m_audioClip;
        [SerializeField] private AudioClip m_audioClipWater;
        [SerializeField] private string m_playerTag = "Player";
        [SerializeField] private float m_timeToDeactivate = 20f;
        private float m_timer = 0f;
        private float m_destroyTimer = 0f;
        private bool m_thrown = false;

        public void Throw(Vector3 force)
        {
            m_rigidbody.AddForce(force);
            m_collider.enabled = true;
            m_thrown = true;
        }

        void Update()
        {
            if (m_thrown)
            {
                if (transform.position.y < -50)
                {
                    m_rigidbody.velocity = Vector3.zero;
                    m_timer = 0f;
                    this.gameObject.SetActive(false);
                }
            }
        }

        IEnumerator Timer()
        {
            m_collider.enabled = false;

            while (m_timer < m_timeToDeactivate)
            {
                m_timer += Time.deltaTime;
                yield return null;
            }

            m_timer = 0f;
            m_rigidbody.velocity = Vector3.zero;
            this.gameObject.SetActive(false);
        }

        private void OnCollisionEnter(Collision collision)
        {
            if (collision.transform.tag != m_playerTag)
            {
                if (transform.tag != "Water")
                    m_audioSource.PlayOneShot(m_audioClip);
                else
                    m_audioSource.PlayOneShot(m_audioClipWater);

                StartCoroutine(Timer());
            }
        }
    }
}
