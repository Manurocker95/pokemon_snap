﻿/// This code is personal and can't be used for commercial games.
/// Non-profit projects can use it with explicit permission and credits
/// 
/// Made by Manuel Rodríguez Matesanz.
/// You can contact me on GBATemp as Manurocker95, email: Manuelrodriguezmatesanz@gmail.com
/// or even on my web: Manuelrodriguezmatesanz.com
/// 

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.IO;
using SimpleJSON;
using Fungus;

namespace PokemonSnapUnity
{
    public class Snap_SaveManager : MonoBehaviour
    {
        #region Variables
        /// <summary>
        /// Singleton Instance for having just one
        /// </summary>
        private static Snap_SaveManager instance;
        public static Snap_SaveManager Instance { get { return instance; } }

        [SerializeField] private bool m_destroyOnLoad = false;
        [SerializeField] private string m_dataPath = "Resources/GameData/GameData";
        [SerializeField] private string m_originalDataPath = "Resources/GameData/OriginalGameData";
        [SerializeField] private string m_gameDataFile = "Data";
        [SerializeField] private string m_saveFilePath;

        [SerializeField] private bool m_existingGame = false;
        [SerializeField] private GameData m_gameData;
        [SerializeField] private GameObject m_continueGO;
        [SerializeField] private GameObject m_warningSaveGame;

        [SerializeField] private string m_defaultTrainerName = "Todd";
        [SerializeField] private Flowchart m_oakFlowChart;

        public GameData GetGameData { get { return m_gameData; } }
        #endregion

        private void Awake()
        {
            if (instance == null)
            {
                instance = this;

                if (!m_destroyOnLoad)
                    DontDestroyOnLoad(this);

                CheckPreviousData();
            }
            else
            {
                Destroy(this.gameObject);
            }
        }

        #region Saving Data Stuff
        /// <summary>
        /// Save Game Data
        /// </summary>
        public void SaveData()
        {
            if (!Directory.Exists(m_dataPath))
            {
                Directory.CreateDirectory(m_dataPath);
            }

            Snap_AlbumManager.Instance.SaveData();
            LoadAlbumData();

            if (!File.Exists(m_saveFilePath))
            {
                string json = Resources.Load(m_originalDataPath).ToString();
                m_gameData.SaveData(json, m_saveFilePath, true);
            }
            else
            {
                m_gameData.SaveData(m_saveFilePath, m_saveFilePath, false);
            }
           

            //string json = JsonUtility.ToJson(m_gameData);
            //File.WriteAllText(m_saveFilePath, json);
        }

        /// <summary>
        /// Load Album Data 
        /// </summary>
        public void LoadAlbumData()
        {
#if UNITY_EDITOR
            List<Snap_AlbumPhoto> d = Snap_AlbumManager.Instance.m_album;

            foreach (Snap_AlbumPhoto photo in d)
            {
                m_gameData.m_albumPhotoList.Add(photo);
            }
#else
            List<Snap_AlbumPhoto> album = m_gameData.m_albumPhotoList;
            Dictionary<string, Snap_AlbumPhoto> d = Snap_AlbumManager.Instance.AlbumDictionary;

            foreach (Snap_AlbumPhoto photo in d.Values)
            {
                album.Add(photo);
            }
#endif
        }
        /// <summary>
        /// Check if we already have a previous game
        /// </summary>
        public void CheckPreviousData()
        {
            m_dataPath = Application.dataPath + "/" + m_dataPath;

            if (!Directory.Exists(m_dataPath))
            {
                Directory.CreateDirectory(m_dataPath);
            }

            m_saveFilePath = m_dataPath + "/" + m_gameDataFile;

            if (File.Exists(m_saveFilePath))
            {
                m_existingGame = true;
                m_continueGO.SetActive(true);
            }
            else
            {
                PlayerPrefs.DeleteAll();
            }
        }
        /// <summary>
        /// Continue our game
        /// </summary>
        public void ContinueData()
        {
            m_oakFlowChart.SetBooleanVariable("initialSpeech", true);
            m_oakFlowChart.SetBooleanVariable("startingGame", false);

            m_gameData = new GameData(m_saveFilePath);

            m_oakFlowChart.SetBooleanVariable("unlockedEverything", m_gameData.m_unlockedEverything);
            m_oakFlowChart.SetBooleanVariable("endedFirstStage", m_gameData.m_tutorialComplete);
            m_oakFlowChart.SetStringVariable("playerName", m_gameData.m_playersName);

            Snap_AlbumManager.Instance.LoadAlbumData();
            LoadAlbumData();
            Snap_MenuManager.Instance.ShowOaksLab();
        }
        /// <summary>
        /// Initialize new game
        /// </summary>
        /// <param name="text"></param>
        public void InitNewGame(Text text)
        {
            m_oakFlowChart.SetBooleanVariable("initialSpeech", true);
            m_oakFlowChart.SetBooleanVariable("startingGame", true);
            SetTrainerName(text);

            if (m_existingGame)
            {
                if (File.Exists(m_saveFilePath))
                {
                    File.Delete(m_saveFilePath);
                }
            }

            Snap_AlbumManager.Instance.LoadOriginalAlbumData();
            LoadAlbumData();
            Snap_MenuManager.Instance.ShowOaksLab();
        }
       
        /// <summary>
        /// Create new Data
        /// </summary>
        public void CreateInitialData()
        {
            m_gameData = new GameData();

            if (m_existingGame)
            {
                m_warningSaveGame.SetActive(true);
            }
            // string json = JsonUtility.ToJson(m_gameData);
            // File.WriteAllText(m_saveFilePath, json);
        }

        /// <summary>
        /// Set trainer name (Easter Eggs can be done here)
        /// </summary>
        /// <param name="name"></param>
        public void SetTrainerName(Text name)
        {
            if (name.text == null || name.text == "" || name.text == " ")
            {
                name.text = m_defaultTrainerName;
            }
               
            m_gameData.m_playersName = name.text;
            m_oakFlowChart.SetStringVariable("playerName", name.text);
        }
        
        /// <summary>
        /// Return GameData Player Name
        /// </summary>
        /// <returns></returns>
        public string GetTrainerName()
        {
            return m_gameData.m_playersName;
        }

        public bool GetUnlockedSignals()
        {
            return m_gameData.m_unlockedSignals;
        }

        public void SetTutorialFungus()
        {
            Debug.Log("Tutorial Completed:" + m_gameData.m_tutorialComplete);
            m_oakFlowChart.SetBooleanVariable("endedFirstStage", m_gameData.m_tutorialComplete);
        }

        public void UnlockAllPoweups()
        {
            m_gameData.m_unlockedSignals = true;
            m_gameData.m_unlockedApple = true;
            m_gameData.m_unlockedFloute = true;
            m_gameData.m_unlockedPesterBall = true;
            m_gameData.m_unlockedEverything = true;
            m_gameData.m_unlockedRun = true;
            m_gameData.m_coursesUnlocked++;
            m_oakFlowChart.SetBooleanVariable("unlockedEverything", true);
        }

#endregion

        [System.Serializable]
        public class GameData
        {
            /// <summary>
            /// Player's Name, set on nameEntry scene
            /// </summary>
            public string m_playersName = "Test";
            /// <summary>
            /// Number of courses unlocked. If one, just Beach is allowed
            /// </summary>
            public int m_coursesUnlocked = 1;
            /// <summary>
            /// Number of Pokémon Discovered. This will change as new pics are taken AND saving the game
            /// </summary>
            public int m_pokemonDiscovered = 0;
            /// <summary>
            /// If the player can take photos of signals
            /// </summary>
            public bool m_unlockedSignals = false;
            /// <summary>
            /// If the player has unlocked the running feature
            /// </summary>
            public bool m_unlockedRun = false;
            /// <summary>
            /// If the player has unlocked the apple feature
            /// </summary>
            public bool m_unlockedApple = false;
            /// <summary>
            /// If the player has unlocked the PesterBall feature
            /// </summary>
            public bool m_unlockedPesterBall = false;
            /// <summary>
            /// If the player has unlocked the Floute feature
            /// </summary>
            public bool m_unlockedFloute = false;
            /// <summary>
            /// If the player has completed the tutorial (First time)
            /// </summary>
            public bool m_tutorialComplete = false;
            /// <summary>
            /// For demo (not to repeat the last speech)
            /// </summary>
            public bool m_unlockedEverything = false;
            /// <summary>
            /// Album photo list. Dictionaries are NOT supported 
            /// </summary>
            public List<Snap_AlbumPhoto> m_albumPhotoList = new List<Snap_AlbumPhoto>();


            public GameData()
            {

            }

            public GameData(string _jsonPath)
            {
                JSONNode info = JSONNode.LoadFromFile(_jsonPath);
                m_unlockedApple = info["m_unlockedApple"].AsBool;
                m_unlockedPesterBall = info["m_unlockedPesterBall"].AsBool;
                m_unlockedFloute = info["m_unlockedFloute"].AsBool;
                m_tutorialComplete = info["m_tutorialComplete"].AsBool;
                m_unlockedEverything = info["m_unlockedEverything"].AsBool;
                m_coursesUnlocked = info["m_coursesUnlocked"].AsInt;
                m_unlockedRun = info["m_unlockedRun"].AsBool;
                m_playersName = info["m_playersName"];
                m_unlockedSignals = info["m_unlockedSignals"].AsBool;

            }

            public void SaveData(string _originalJsonPath, string _destJsonPath, bool original)
            {
                JSONNode info;

                if (original)
                    info = JSONNode.Parse(_originalJsonPath);
                else
                    info = JSONNode.LoadFromFile(_originalJsonPath);

                info["m_playersName"] = m_playersName.ToString();
                info["m_unlockedApple"] = m_unlockedApple.ToString();
                info["m_unlockedPesterBall"] = m_unlockedPesterBall.ToString();
                info["m_unlockedFloute"] = m_unlockedFloute.ToString();
                info["m_tutorialComplete"] = m_tutorialComplete.ToString();
                info["m_unlockedEverything"] = m_unlockedEverything.ToString();
                info["m_coursesUnlocked"] = m_coursesUnlocked.ToString();
                info["m_unlockedRun"] = m_unlockedRun.ToString();
                info["m_unlockedSignals"] = m_unlockedSignals.ToString();

                info.SaveToFile(_destJsonPath);
            }
        }
    }
}
