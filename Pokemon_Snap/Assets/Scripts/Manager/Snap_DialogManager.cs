﻿/// This code is personal and can't be used for commercial games.
/// Non-profit projects can use it with explicit permission and credits
/// 
/// Made by Manuel Rodríguez Matesanz.
/// You can contact me on GBATemp as Manurocker95, email: Manuelrodriguezmatesanz@gmail.com
/// or even on my web: Manuelrodriguezmatesanz.com
/// 

using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System;
using System.IO;
using UnityStandardAssets.Characters.FirstPerson;

namespace PokemonSnapUnity
{
    public class Snap_DialogManager : MonoBehaviour
    {
        //Singleton
        private static Snap_DialogManager instance;
        public static Snap_DialogManager Instance { get { return instance; } }

        [SerializeField] private TextAsset[] m_gameScripts;
        [SerializeField] private Text m_subsText;
        [SerializeField] private GameObject m_SubsBackground;           //Referencia al fondo de los subtítulos.
        [SerializeField] private bool m_subtitlesActivated = true;
        [SerializeField] private float m_minPhraseDuration = 0.5f;              //Duración minima de un subtítulo.
        [SerializeField] private float m_PhraseDurationIncreasePerWord = 0.01f; //Incremento de duración del texto por letra
        [SerializeField] private float m_SubsFadeSpeed = 1.5f;                  //Velocidad del efecto de los subtítulos

        private Snap_TextParser m_oParseXML;                                    //Instancia del objeto que parsea el XML                                          //Indica si está en modo tutorial
        private bool m_bTakeItemsOnDialogEnds = false;                          //Indica si se contienen objetos almacenados para añadir al inventario tras un diálogo.
        private bool m_bIsPlayingPhrase = false;                                //Indica si se está reproduciendo una frase.
        private bool m_bBreaking = false;
        private SystemLanguage m_language = SystemLanguage.English;

        //properties
        public bool SubtitlesActivated { set { m_subtitlesActivated = value; } get { return m_subtitlesActivated; } }
        public bool isPlayingPhrase { get { return m_bIsPlayingPhrase; } }
        public bool takeItemsOnDialogEnds { get { return m_bTakeItemsOnDialogEnds; } set { m_bTakeItemsOnDialogEnds = value; } }
        public Snap_TextParser ParseXML { get { return m_oParseXML; } }
        public bool Break { set { m_bBreaking = value; } get { return m_bBreaking; } }
        public SystemLanguage Language { set { m_language = value; m_oParseXML.SetLang(value); } get { return m_language; } }

        private void Awake()
        {
            if (instance == null)
            {
                instance = this;
            }
            else
            {
                Debug.LogError("[UHMDialogManager] Error! There can only be one instance of UHMDialogManager");
            }
        }

        public void InitializeDialogManager()
        {
            m_language = Snap_SettingsManager.Instance.GameLanguage;

            m_oParseXML = new Snap_TextParser();
            m_oParseXML.SetLang(m_language);

            foreach (TextAsset gameScript in m_gameScripts)
            {
                m_oParseXML.ParseScript(gameScript);
            }
        }

        private void Start()
        {

        }

        public void PlayRowFromScript(string context, float timeToFade = -1f, string audioName = "")
        {
            StopAllCoroutines();
            m_bIsPlayingPhrase = true;

            if (context.CompareTo("") != 0)
            {
                // Le pasamos manualmente un audio
                if (audioName.CompareTo("") == 0)
                {
                    if (m_oParseXML.SayPhrase(context).AudioName.CompareTo("") != 0)
                    {
                        string path = "Sounds/Dialogs/" + m_oParseXML.SayPhrase(context).AudioName;
                        AudioClip audio = (AudioClip)Resources.Load(path);

                        if (audio)  // Si existe el audio
                        {
                            AudioSource aus = GameObject.Find("audio_Dialog").GetComponent<AudioSource>();
                            aus.clip = audio;
                            aus.Play();
                        }
                        else
                        {
                            Debug.Log("Can't find " + path);
                        }
                    }
                    else
                    {
                        Debug.Log("No Audio Asociated for " + context);
                    }
                }

                if (!m_subtitlesActivated)
                {
                    m_bIsPlayingPhrase = false;
                    return;
                }

                StartCoroutine(_PlayPhraseCorroutine(m_oParseXML.SayPhrase(context), timeToFade));
            }
            else
            {
                Debug.Log("NO SE HA ENCONTRADO EL NOMBRE DEL OBJETO.");
                m_bIsPlayingPhrase = false;
            }

        }

        IEnumerator _PlayPhraseCorroutine(Snap_InfoText phraseToPlay, float timeToFade)
        {
            if (phraseToPlay == null || phraseToPlay.Text == "" || m_bBreaking) //Si no hay mensaje, no muestra nada
            {
                m_bBreaking = false;
                Debug.Log("BReak");
                yield break;
            }

            //Iguala el alpha del color a 0 para crear su efecto. 
            Color subsColor = m_subsText.color;

            //Reproduce un párrafo de la frase cada vez.
            if (phraseToPlay != null)
            {

                //Activa el fondo de los subtítulos
                m_SubsBackground.SetActive(true);

                m_subsText.color = new Color(subsColor.r, subsColor.g, subsColor.b, 0f);

                if (m_bBreaking) //Si no hay mensaje, no muestra nada
                {
                    m_bBreaking = false;
                    yield break;
                }

                for (int i = 0; i < phraseToPlay.NumberOfRows; i++)
                {
                    if (m_bBreaking) //Si no hay mensaje, no muestra nada
                    {
                        m_bBreaking = false;
                        m_subsText.color = new Color(subsColor.r, subsColor.g, subsColor.b, 0f);
                        yield break;
                    }

                    //Frase actual a reproducir.
                    m_subsText.text = phraseToPlay.TextRows[i];

                    //Efecto de aparición de los subtítulos.
                    bool fading = true;
                    while (fading)
                    {
                        if (m_bBreaking) //Si no hay mensaje, no muestra nada
                        {
                            m_bBreaking = false;
                            m_subsText.color = new Color(subsColor.r, subsColor.g, subsColor.b, 0f);
                            yield break;
                        }

                        subsColor = m_subsText.color;
                        float lerpedValue = Mathf.Lerp(subsColor.a, 1.3f, Time.deltaTime * m_SubsFadeSpeed);
                        m_subsText.color = new Color(subsColor.r, subsColor.g, subsColor.b, lerpedValue);

                        if (lerpedValue >= 0.95f)
                        {
                            fading = false;
                            m_subsText.color = new Color(subsColor.r, subsColor.g, subsColor.b, 1f);
                        }
                        yield return null;
                    }

                    //Tiempo a esperar de este párrafo
                    if (timeToFade == -1)
                        yield return new WaitForSeconds(CalcRowDuration(phraseToPlay.TextRows[i]));
                    else
                        yield return new WaitForSeconds(timeToFade);

                    //Efecto de desaparición de los subtítulos.
                    fading = true;
                    while (fading)
                    {
                        if (m_bBreaking) //Si no hay mensaje, no muestra nada
                        {
                            m_bBreaking = false;
                            m_subsText.color = new Color(subsColor.r, subsColor.g, subsColor.b, 0f);
                            yield break;
                        }

                        subsColor = m_subsText.color;
                        float lerpedValue = Mathf.Lerp(subsColor.a, -0.3f, Time.deltaTime * m_SubsFadeSpeed);
                        m_subsText.color = new Color(subsColor.r, subsColor.g, subsColor.b, lerpedValue);

                        if (lerpedValue <= 0.05f)
                        {
                            fading = false;
                            m_subsText.color = new Color(subsColor.r, subsColor.g, subsColor.b, 0f);
                        }
                        yield return null;
                    }
                }
            }
            else
            {
                Debug.LogError("[UHMDialogManager] There's an item without phrase. Ignoring and going to next");
            }

            //Recoge los objetos guardados si los hay.
            if (m_bTakeItemsOnDialogEnds)
            {
                m_bTakeItemsOnDialogEnds = false;
            }

            m_bBreaking = false;
        }

        float CalcRowDuration(string row)
        {
            return Mathf.Clamp(row.Length * m_PhraseDurationIncreasePerWord, m_minPhraseDuration, 10.0f);
        }

        void setTextPosition(float x, float y)
        {
            m_subsText.rectTransform.position = new Vector3(x, y, 0);
        }

        void setTextSize(int size)
        {
            m_subsText.fontSize = size;
        }
    }

}