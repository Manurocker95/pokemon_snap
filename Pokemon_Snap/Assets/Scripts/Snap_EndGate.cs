﻿/// This code is personal and can't be used for commercial games.
/// Non-profit projects can use it with explicit permission and credits
/// 
/// Made by Manuel Rodríguez Matesanz.
/// You can contact me on GBATemp as Manurocker95, email: Manuelrodriguezmatesanz@gmail.com
/// or even on my web: Manuelrodriguezmatesanz.com
/// 

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PokemonSnapUnity
{
    /// <summary>
    /// Script placed to the end gate
    /// </summary>
    [RequireComponent(typeof(Collider))]
    public class Snap_EndGate : MonoBehaviour
    {
        /// <summary>
        /// if an object with this tag collides with the gate, we call the PhotoManager to end the course 
        /// </summary>
        [SerializeField] private string m_playerTag = "Player";

        /// <summary>
        /// On trigger enter
        /// </summary>
        /// <param name="other"></param>
        private void OnTriggerEnter(Collider other)
        {
            if (other.tag == m_playerTag)
            {
                Snap_PhotoManager.Instance.EndCourse();
            }
        }
    }
}

